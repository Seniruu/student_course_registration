<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Student;


class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('table', [
            'students' => $students
        ]);
    }


    public function create()
    {
        $courses = Course::all();
        return view('form',[
            'courses' => $courses
        ]);
    }


    public function store(Request $request)
    {
        $student = new Student();
        $student->first_name = $request->first_name;
        $student->last_name= $request->last_name;
        $student->email = $request->email;
        $student->course_id = $request->course_id;
        $student->save();

        return redirect('students-information');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
