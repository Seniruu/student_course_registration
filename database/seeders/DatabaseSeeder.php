<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

            //creating courses
            DB::table('courses')->insert([
                ['name' => 'Computer Science'],
                ['name' => 'Business Information System'],
                ['name' => 'Software Engineering'],
            ]);

    }
}
