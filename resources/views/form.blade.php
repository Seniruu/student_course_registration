<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-B">
    <meta name="viewport" content="widthh+device-width, initial-scale=1.0">
    <title>Student Application</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="jumbotron">
    <h1 class="text-center"> Student Application </h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 col-sm-12">
            <form action="" method="POST">
                @csrf
                <div class="form group">
                    <label>First Name: </label>
                    <input type="text" required name="first_name"  placeholder="Dave" >
                </div>
                <div class="form group">
                    <label>Last Name: </label>
                    <input type="text" required name="last_name" placeholder="Perera">
                </div>
                <div class="form group">
                    <label>Email: </label>
                    <input type="email" required name="email"  placeholder="dave@gmail.com">
                </div>
                <div>
                    <label>Course: </label>
                    <select name="course_id">
                        @foreach($courses as $course)
                            <option value="{{$course->id}}">{{$course->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit">Add student</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
