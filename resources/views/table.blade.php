<h1>Student data</h1>

<table border="1">
    <tr>
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Course</th>
    </tr>
    @foreach($students as $student)
        <tr>
            <td>{{ $student->first_name }}</td>
            <td>{{ $student->last_name }}</td>
            <td>{{ $student->email }}</td>
            <td>{{ $student->course->name }}</td>
        </tr>
    @endforeach


</table>
